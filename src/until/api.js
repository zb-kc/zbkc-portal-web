import {get, post} from './request'
export function login (data) {
  return post('/zbkc/userInfo/login', data)
}
//
// 组织管理
//

export function grouplist () {
  return post('/zbkc/sysOrg/list')
}
export function groupupdate (obj) {
  return post('/zbkc/sysOrg/upd', obj)
}
export function useralllist (obj) {
  return post('/zbkc/sysOrg/user', obj)
}
export function useralllistid (obj) {
  return post('/zbkc/sysOrg/selfOrg/' + obj)
}
export function rolegrant (obj) {
  return post('/zbkc/sysOrg/grant', obj)
}
export function rolegrantadd (obj) {
  return post('/zbkc/sysOrg/add', obj)
}
export function groupstatus (obj) {
  return get('/zbkc/sysOrg/status?' + obj)
}
//
// 菜单管理接口
//
export function menulist () {
  return post('/zbkc/sysMenu/list')
}
export function menulistinput (name) {
  return get('/zbkc/sysMenu/input?name=' + name)
}
export function menuadd (obj) {
  return post('/zbkc/sysMenu/add', obj)
}
export function menuupdate (obj) {
  return post('/zbkc/sysMenu/upd', obj)
}
export function menustatus (obj) {
  return get('/zbkc/sysMenu/status?' + obj)
}

//
// 动态菜单列表
//
export function menuList (userId) {
  return get('/zbkc/sysUser/selfMenu/' + userId)
}

//
// 用户管理接口
//
export function userlist () {
  return get('/zbkc/sysUser/all')
}
export function useradd (obj) {
  return post('/zbkc/sysUser/add', obj)
}
export function userupdate (obj) {
  return post('/zbkc/sysUser/upd', obj)
}
export function userstatus (status) {
  return get('/zbkc/sysUser/status?' + status)
}
export function updaterole (obj) {
  return get('/zbkc/sysUser/updRole?' + obj)
}
export function userinput (name) {
  return get('/zbkc/sysUser/input?name=' + name)
}
export function userpage (obj) {
  return post('/zbkc/sysUser/page', obj)
}
export function userrole (id) {
  return post('/zbkc/sysUser/selfOrg/' + id)
}
export function grantadd (obj) {
  return post('/zbkc/sysUser/grant', obj)
}
//
// 角色管理接口
//
export function jurisdictionadd (obj) {
  return post('/zbkc/sysRoleMenuFunc/grant', obj)
}
export function jurisdiction () {
  return post('/zbkc/sysRoleMenuFunc/list')
}
export function jurisdictionid (id) {
  return post('/zbkc/sysRoleMenuFunc/self/' + id)
}
export function rolelist () {
  return get('/zbkc/sysRole/list')
}
export function roleadd (obj) {
  return post('/zbkc/sysRole/add', obj)
}
export function roleupdate (obj) {
  return post('/zbkc/sysRole/upd', obj)
}
export function pagelist (obj) {
  return post('/zbkc/sysRole/page', obj)
}
export function rolestatus (obj) {
  return get('/zbkc/sysRole/status?' + obj)
}
export function config () {
  return get('/zbkc/sysDataValue/list')
}
export function roleinput (name) {
  return get('/zbkc/sysRole/input?name=' + name)
}
//
// 数据字典接口
//
export function dictypelist () {
  return get('/zbkc/sysDataType/list')
}
export function dictypeinput (name) {
  return get('/zbkc/sysDataType/input?name=' + name)
}
export function dictypeadd (obj) {
  return post('/zbkc/sysDataType/add', obj)
}
export function dictypeupdate (obj) {
  return post('/zbkc/sysDataType/upd', obj)
}
export function dictypestatus (obj) {
  return get('/zbkc/sysDataType/status?' + obj)
}
export function dictypepage (obj) {
  return post('/zbkc/sysDataType/page', obj)
}
//
export function dicvaluelist () {
  return get('/zbkc/sysDataValue/list')
}
export function dicvalueeinput (name) {
  return get('/zbkc/sysDataValue/input?name=' + name)
}
export function dicvalueadd (obj) {
  return post('/zbkc/sysDataValue/add', obj)
}
export function dicvalueupdate (obj) {
  return post('/zbkc/sysDataValue/upd', obj)
}
export function dicvaluestatus (obj) {
  return get('/zbkc/sysDataValue/status?' + obj)
}
export function dicvaluespage (obj) {
  return get('/zbkc/sysDataValue/page?' + obj)
}

//
// 系统参数接口
//
export function configinput (name) {
  return get('/zbkc/sysParamConfig/input?name=' + name)
}
export function configpage (obj) {
  return get('/zbkc/sysParamConfig/page?' + obj)
}
export function configadd (obj) {
  return post('/zbkc/sysParamConfig/add', obj)
}
export function configupdate (obj) {
  return post('/zbkc/sysParamConfig/upd', obj)
}
export function configstatus (obj) {
  return get('/zbkc/sysParamConfig/status?' + obj)
}

export function wyBasicInfoList (obj) {
  return post('/zbkc/wyBasicInfo/list', obj)
}

//
// 租赁社会物业
//

// 租赁社会物业-物业列表
export function getRentList (obj) {
  return post('/zbkc/wyBasicInfo/property/list', obj)
}
// 租赁社会物业-新增租赁社会物业
export function addRentList (obj) {
  return post('/zbkc/wyBasicInfo/property/add', obj)
}
// 租赁社会物业-新增-获取物业编码
export function getRentCode () {
  return get('/zbkc/wyBasicInfo/property/code')
}
// 租赁社会物业-修改租赁社会物业
export function updRentList (obj) {
  return post('/zbkc/wyBasicInfo/property/upd', obj)
}
// 租赁社会物业-物业详情
export function getRentListDetail (id) {
  return get('/zbkc/wyBasicInfo/property/social/' + id)
}
// 租赁社会物业-删除租赁社会物业
export function deleteRentList (idArr) {
  return post('/zbkc/wyBasicInfo/property/del?id=' + idArr)
}

//
// 业务办理
//

// 业务办理-待办/所有业务列表/查询
export function getHandleList (obj) {
  return post('/zbkc/ywBusiness/yw/list', obj)
}
// 业务办理-待办/所有业务列表-导出
export function exportBusiness (obj) {
  return post('/zbkc/ywBusiness/yw/export', obj)
}
// 业务办理-待办/所有业务列表-移交用房申请办理/合同签署申请办理/物业资产申报
export function businessHandle (id) {
  return get('/zbkc/ywBusiness/yw/dealWith?businessId=' + id)
}
// 业务办理-待办/所有业务列表-获取流程进度图
export function getProcessImg (id) {
  return get('/zbkc/ywBusiness/yw/getProcessImg?businessId=' + id)
}

// 业务办理-短信管理-短信模板列表
export function getMessageList (obj) {
  return post('/zbkc/wySysMsg/list', obj)
}
// 业务办理-短信管理-新增短信模版
export function addMessage (obj) {
  return post('/zbkc/wySysMsg/addMsg', obj)
}
// 业务办理-短信管理-修改短信模版
export function updMessage (obj) {
  return post('/zbkc/wySysMsg/upd', obj)
}
// 业务办理-短信管理-查看当前行详情
export function getRowDetail (id) {
  return get('/zbkc/wySysMsg/row/' + id)
}

// 运营管理
export function entrustManagementList (obj) {
  return post('/zbkc/yyCommissioneOperation/list', obj)
}
export function usageRegistion (obj) {
  return post('/zbkc/yyUseReg/list', obj)
}
export function addOperation (obj) {
  return post('/zbkc/yyCommissioneOperation/addOperation', obj)
}
export function updOperation (obj) {
  return post('/zbkc/yyCommissioneOperation/updOperation', obj)
}

// 合同管理
export function yyContractList (obj) {
  return post('/zbkc/yyContract/list', obj)
}
export function addContract (obj) {
  return post('/zbkc/yyContract/addContract', obj)
}
export function seeList (obj) {
  return get('/zbkc/yyContract/row?id=' + obj)
}
// 押金管理
export function homeList (obj) {
  return post('/zbkc/yyDeposit/homeList', obj)
}
export function homeListId (obj) {
  return get('/zbkc/yyDeposit/seeRow/' + obj)
}
export function homeEdit (obj) {
  return post('/zbkc/yyDeposit/edit', obj)
}
// 上次文件接口
export function uploadImage (obj) {
  return post('/zbkc/fileUpload/upload', obj)
}

// 资产管理
export function allProRight () {
  return post('/zbkc/wyProRightReg/allProRight')
}
// 常用报表
export function contractList (obj) {
  return post('/zbkc/yyPreContract/contractList', obj)
}
export function industrylist (obj) {
  return post('/zbkc/yyPreContract/industRevieew', obj)
}
export function emptylist (obj) {
  return post('/zbkc/yyPreContract/emptyDetail', obj)
}
export function leaselist (obj) {
  return post('/zbkc/yyPreContract/leaseDetail', obj)
}
export function exportLease (obj) {
  return post('/zbkc/yyPreContract/exportLease', obj)
}
// 获取标签
export function selectTag (obj) {
  return get('/zbkc/sysDataType/code/' + obj)
}
// 预警提醒
export function remindList (obj) {
  return post('/zbkc/yjSmsRecord/list', obj)
}
export function mesRecord (obj) {
  return post('/zbkc/yjSmsRecord/warnRecord', obj)
}
