import { stat } from "../request";
export default {
  namespaced: true, // 命名空间
  state: {
    settledCount: 0,//我的入驻
    collectCount: 0,//我的收藏
    contractCount: 0,//我的合同
    recordCount: 0,//申请记录
  },
  getters: {
  },
  mutations: {
    setSettledCount(state, payload) {
      state.settledCount = payload
    },
    setCollectCount(state, payload) {
      state.collectCount = payload
    },
    setContractCount(state, payload) {
      state.contractCount = payload
    },
    setRecordCount(state, payload) {
      state.recordCount = payload
    },
  },
  actions: {
    // asyncMinus(context, payload) {
    //   setTimeout(() => {
    //     // 异步操作，假设这里在发送ajax请求
    //     context.commit('minus', payload)
    //   }, 2000)
    // },
    //刷新个人中心菜单中的数量统计
    refreshCount(context) {
      let params = {
        socialUniformCreditCode: sessionStorage.getItem("creditCode"),
      };
      stat(params).then((res) => {
        //console.log(res);
        if (res.data && res.data.data) {
          context.commit('setSettledCount', res.data.data.myHouseNumber || 0)
          context.commit('setCollectCount', res.data.data.myFavoriteNumber || 0)
          context.commit('setContractCount', res.data.data.myContractNumber || 0)
          context.commit('setRecordCount', res.data.data.myApplyNumber || 0)
        }
      });
    }
  }
}
