import { get, post } from '@/until/request'

export function list (userid) {
  return get('/portal/wyBasic/myEnter/' + userid)
}
