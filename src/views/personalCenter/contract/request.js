import { get, post } from '@/until/request'

export function signList (obj) {
  return post('/portal/contact/contactSignList', obj)
}

export function myContractList (obj) {
  return post('/portal/contact/myContactList', obj)
}
export function contractInfo (obj) {
  return post('/portal/contact/contactSignApply', obj)
}
export function apply (obj) {
  return post('/portal/contact/apply', obj)
}
export function removeFile (obj) {
  return get('/portal/contact/fileDelete' + obj)
}
export function downloadFile (obj) {
  return get('/portal/contact/fileDownload' + obj)
}
export function details (obj) {
  return post('/portal/contact/contactInfo', obj)
}
