import {
  get,
  post
} from '@/until/request'

export function list (obj) {
  return post('/portal/wyBasic/myCollection', obj)
}
