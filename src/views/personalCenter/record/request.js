import { get, post } from '@/until/request'

export function list (obj) {
  return post('/portal/contact/applyList', obj)
}
export function details (obj) {
  return post('/portal/contact/applyInfo', obj)
}
export function download (obj) {
  return get('/portal/contact/fileDownload' + obj)
}
export function undo (obj) {
  return post('/portal/contact/applyUndo', obj)
}
