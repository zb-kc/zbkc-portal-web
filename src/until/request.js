
import axios from 'axios'
import { MessageBox } from 'element-ui'
const http = axios.create({
  headers: {
    'Content-Type': 'application/json;charset=UTF-8',
    'Authorization': 'bearer ' + window.sessionStorage.getItem('token')
  },
  timeout: 30000,
  baseURL: '/'
  // baseURL:'http://192.168.1.76:10010/api/v1/zbkc-portal/'
  // baseURL: 'http://192.168.1.4:10005/api/v1/zbkc/'
})

// 添加请求拦截器
http.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  if (window.sessionStorage.getItem('token')) {
    config.headers.Authorization = window.sessionStorage.getItem('token')
  }

  // console.log(window.localStorage.getItem('token'))

  if (config.method === 'get') {
    config.data = { unused: 0 } // 这个是关键点，加入这行就可以了,解决get,请求添加不上Content-Type
  }
  config.headers['Content-type'] = 'application/json;charset=UTF-8'

  return config
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error)
})

// 添加响应拦截器
http.interceptors.response.use(function (res) {
  // console.log(1)
  // 对响应数据做点什么
  if (res.data.code === 1006 || res.data.code === 500 || res.data.code === 1005) {
    MessageBox.alert('登陆信息超时，请重新登陆', '登陆超时', {
      confirmButtonText: '跳转',
      callback: action => {
        window.location.href = '/'
      }
    })
  } else if (res.data.token) { // 判断token是否存在，如果存在说明需要更新token
    sessionStorage.setItem('token', res.data.data.accessToken)// 覆盖原来的token(默认一天刷新一次)
  }
  return res
}, function (error) {
  // 对响应错误做点什么
  return Promise.reject(error)
})

export function get (url, data = {}) {
  return new Promise((resolve, reject) => {
    http.get(url)
    // , {
    //   params: data
    // }
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err)
      })
  })
}
export function post (url, data = {}) {
  return new Promise((resolve, reject) => {
    http.post(url, data).then(res => {
      resolve(res)
    }, err => reject(err))
  })
}

export function request (methed, url, data = {}, headers) {
  return new Promise((resolve, reject) => {
    http({
      method: methed || 'post',
      url: url,
      params: methed === 'get' ? data : '',
      data: methed !== 'get' ? data : '',
      headers: headers || {
        'Content-Type': 'application/json'
      }
    })
      .then(response => {
        // 对接口错误码做处理
        resolve(response.data)
      })
      .catch(err => {
        reject(err)
      })
  })
}
