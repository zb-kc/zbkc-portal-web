import { get, post } from '@/until/request'

export function user (userid) {
  return get('/portal/wyBasic/myEnter/' + userid)
}

// 数量统计
export function stat (obj) {
  return post('/portal/contact/myselfCenter', obj)
}
