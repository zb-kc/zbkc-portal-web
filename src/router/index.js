import Vue from 'vue'
import Router from 'vue-router'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

Vue.use(Router)
const router = new Router({
  linkExactActiveClass: 'act',
  mode: 'history',
  routes: [
    {
      path: '/',
      component: () => import('@/views/login/index.vue')
      // component: () => import('@/views/personalCenter/settled/index.vue')
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('@/views/home/index.vue')
    },
    {
      path: '/personalCenter',
      name: 'personalCenter',
      redirect: '/personalCenter/settled',
      component: () => import('@/views/personalCenter/index.vue'),
      children: [
        {
          name: 'settled',
          path: '/personalCenter/settled',
          component: () => import('@/views/personalCenter/settled/index.vue')
        },
        {
          name: 'collect',
          path: '/personalCenter/collect',
          component: () => import('@/views/personalCenter/collect/index.vue')
        },
        {
          name: 'contract',
          path: '/personalCenter/contract',
          component: () => import('@/views/personalCenter/contract/index.vue')
        },
        {
          name: 'record',
          path: '/personalCenter/recordList',
          component: () => import('@/views/personalCenter/record/index.vue')
        }
      ]
    },
    {
      name: 'signList',
      path: '/signList',
      component: () => import('@/views/personalCenter/contract/signList.vue')
    },
    {
      name: 'sign',
      path: '/sign',
      component: () => import('@/views/personalCenter/contract/sign.vue')
    },
    {
      name: 'propertyList',
      path: '/propertyList',
      component: () => import('@/views/property/propertyList.vue')
    },
    {
      name: 'propertyDetails',
      path: '/propertyDetails',
      component: () => import('@/views/property/propertyDetails.vue')
    },
    {
      name: 'recordDetails',
      path: '/recordDetails',
      component: () => import('@/views/personalCenter/record/details.vue')
    },
    {
      name: 'contractDetails',
      path: '/contractDetails',
      component: () => import('@/views/personalCenter/contract/details.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/login/index.vue')
    },
    // 用房申请
    {
      path: '/houseApply',
      name: 'houseApply',
      component: () => import('@/views/house/apply.vue')
    }

  ]
})

// 注册导航前置守卫
/*
    to:去哪
    from:从哪来
    to和from都有一个属性叫path，代表路径
    next:是一个函数，调用这个函数就代表放行，不调用就是不放行
        next不传参数就代表你以前想去哪还去哪，如果传参，就代表把你放行到你指定的路径
*/
router.beforeEach((to, from, next) => {
  NProgress.start()
  if (to.path === '/' || to.path === '/login') {
    NProgress.done()
    // 登录页 不需要判断
    next()
  } else {
    // let tokenStr = sessionStorage.getItem('token')
    // if (tokenStr) {
    //   NProgress.done()
    //   // 如果不需要，则直接跳转到对应路由
    //   next()
    // } else {
    //   // 如果需要，则跳转到登录页
    //   Vue.prototype.$message.error('请先登录！')
    //   next('/login')
    //   NProgress.done()
    // }
    let code = sessionStorage.getItem('creditCode')
    if (code) {
      NProgress.done()
      next()
    } else {
      Vue.prototype.$message.error('请先登录！')
      next('/login')
      NProgress.done()
    }
    next()
    NProgress.done()
  }
})

router.afterEach(() => {
  NProgress.done()
})

export default router
