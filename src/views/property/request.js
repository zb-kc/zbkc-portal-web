import {
  get,
  post
} from '@/until/request'

export function details (id) {
  return get(`/portal/wyBasic/showOne/${id}/${sessionStorage.getItem('creditCode')}`)
}
export function goverWyList (obj) {
  return post('/portal/wyBasic/goverWy', obj)
}
// 物业列表
export function list (obj) {
  return post('/portal/wyBasic/showMore', obj)
}

// 收藏/取消收藏
export function collect (obj) {
  return post('/portal/wyBasic/collect', obj)
}
