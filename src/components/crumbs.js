export default {
// 资产登记（数据字典对应面包屑）
  '1': [
    { name: '资产登记', url: '/estate' },
    { name: '园区物业新增' }
  ],
  '1-1': [
    { name: '资产登记', url: '/estate' },
    { name: '园区物业编辑' }
  ],
  '1-2': [
    { name: '资产登记', url: '/estate' },
    { name: '园区物业查看' }
  ],
  '2': [
    { name: '资产登记', url: '/estate' },
    { name: '楼栋新增' }
  ],
  '2-1': [
    { name: '资产登记', url: '/estate' },
    { name: '楼栋编辑' }
  ],
  '2-2': [
    { name: '资产登记', url: '/estate' },
    { name: '楼栋查看' }
  ],
  '3': [
    { name: '资产登记', url: '/estate' },
    { name: '楼层新增' }
  ],
  '3-1': [
    { name: '资产登记', url: '/estate' },
    { name: '楼层编辑' }
  ],
  '3-2': [
    { name: '资产登记', url: '/estate' },
    { name: '楼层查看' }
  ],
  '4': [
    { name: '资产登记', url: '/estate' },
    { name: '单元新增' }
  ],
  '4-1': [
    { name: '资产登记', url: '/estate' },
    { name: '单元编辑' }
  ],
  '4-2': [
    { name: '资产登记', url: '/estate' },
    { name: '单元查看' }
  ],
  '5': [
    { name: '资产登记', url: '/estate' },
    { name: '零散物业新增' }
  ],
  '5-1': [
    { name: '资产登记', url: '/estate' },
    { name: '零散物业编辑' }
  ],
  '5-2': [
    { name: '资产登记', url: '/estate' },
    { name: '零散物业查看' }
  ],
  '6': [
    { name: '资产登记', url: '/estate' },
    { name: '楼栋物业新增' }
  ],
  '6-1': [
    { name: '资产登记', url: '/estate' },
    { name: '楼栋物业编辑' }
  ],
  '6-2': [
    { name: '资产登记', url: '/estate' },
    { name: '楼栋物业查看' }
  ]
}
