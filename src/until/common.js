export function formatDate (value) {
  let date = new Date(value)
  let y = date.getFullYear()
  let MM = date.getMonth() + 1
  MM = MM < 10 ? '0' + MM : MM
  let d = date.getDate()
  d = d < 10 ? '0' + d : d
  let h = date.getHours()
  h = h < 10 ? '0' + h : h
  let m = date.getMinutes()
  m = m < 10 ? '0' + m : m
  let s = date.getSeconds()
  s = s < 10 ? '0' + s : s
  return y + '-' + MM + '-' + d + ' ' + h + ':' + m + ':' + s
}
export function formatDate1 (value) {
  let date = new Date(value)
  let y = date.getFullYear()
  let MM = date.getMonth() + 1
  MM = MM < 10 ? '0' + MM : MM
  let d = date.getDate()
  d = d < 10 ? '0' + d : d
  return y + '-' + MM + '-' + d
}
export function numberFormat (value) {
  let unit = ''
  var k = 10000
  let sizes = ['', '万', '亿', '万亿']
  let i
  if (value < k) {
    value = value
  } else {
    i = Math.floor(Math.log(value) / Math.log(k))
    value = ((value / Math.pow(k, i))).toFixed(2)
    unit = sizes[i]
  }
  return value + unit
}
