import {get, post} from './request'
//
// 菜单管理接口
//

//
// 资产管理——产权登记
//

// export function rigisterpage (page) {
//   // console.log(data)
//   return get('/zbkc/wyProRightReg/page?' + page)
// }
export function rigisterinput (name) {
  return get('/zbkc/wyProRightReg/input?name=' + name)
  // console.log('/zbkc/sysMenu/input?name=' + name)
}
export function rigisteradd (obj) {
  // console.log(obj)
  return post('/zbkc/wyProRightReg/addProRight', obj)
}
export function rigisterupdate (obj) {
  return post('/zbkc/wyProRightReg/upd', obj)
}
export function rigisterstatus (obj) {
  return get('/zbkc/wyProRightReg/status?' + obj)
}

export function wyProRightRegList (obj) {
  return post('/zbkc/wyProRightReg/list', obj)
}
//
// 基本信息
//
export function informationlist (obj) {
  return post('/zbakc/wyBasicInfo/page', obj)
}
