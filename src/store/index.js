import Vue from 'vue'
import Vuex from 'vuex'

// 导入模块
import personalCenter from './modules/personalCenter'
import user from './modules/user'

Vue.use(Vuex)

export default new Vuex.Store({
  // 对模块进行注册
  modules: {
    personalCenter,
    user
  }
})
